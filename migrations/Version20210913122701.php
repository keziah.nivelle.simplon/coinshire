<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210913122701 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE configurations_components');
        $this->addSql('ALTER TABLE configurations DROP FOREIGN KEY FK_31C6AD9D382E9BD');
        $this->addSql('DROP INDEX IDX_31C6AD9D382E9BD ON configurations');
        $this->addSql('ALTER TABLE configurations DROP creat_by_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE configurations_components (configurations_id INT NOT NULL, components_id INT NOT NULL, INDEX IDX_FBAFF1846F0A14EE (configurations_id), INDEX IDX_FBAFF184CA91F907 (components_id), PRIMARY KEY(configurations_id, components_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE configurations_components ADD CONSTRAINT FK_FBAFF1846F0A14EE FOREIGN KEY (configurations_id) REFERENCES configurations (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE configurations_components ADD CONSTRAINT FK_FBAFF184CA91F907 FOREIGN KEY (components_id) REFERENCES components (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE configurations ADD creat_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE configurations ADD CONSTRAINT FK_31C6AD9D382E9BD FOREIGN KEY (creat_by_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_31C6AD9D382E9BD ON configurations (creat_by_id)');
    }
}
