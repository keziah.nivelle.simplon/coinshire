<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210903134927 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE configurations DROP FOREIGN KEY FK_31C6AD9A76ED395');
        $this->addSql('DROP INDEX IDX_31C6AD9A76ED395 ON configurations');
        $this->addSql('ALTER TABLE configurations DROP user_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE configurations ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE configurations ADD CONSTRAINT FK_31C6AD9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_31C6AD9A76ED395 ON configurations (user_id)');
    }
}
