<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210903133838 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE configurations_gpu');
        $this->addSql('ALTER TABLE configurations DROP FOREIGN KEY FK_31C6AD9A76ED395');
        $this->addSql('DROP INDEX IDX_31C6AD9A76ED395 ON configurations');
        $this->addSql('ALTER TABLE configurations DROP user_id, DROP date');
        $this->addSql('ALTER TABLE user ADD configurations_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6496F0A14EE FOREIGN KEY (configurations_id) REFERENCES configurations (id)');
        $this->addSql('CREATE INDEX IDX_8D93D6496F0A14EE ON user (configurations_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE configurations_gpu (configurations_id INT NOT NULL, gpu_id INT NOT NULL, INDEX IDX_9180550A6F0A14EE (configurations_id), INDEX IDX_9180550A98003202 (gpu_id), PRIMARY KEY(configurations_id, gpu_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE configurations_gpu ADD CONSTRAINT FK_9180550A6F0A14EE FOREIGN KEY (configurations_id) REFERENCES configurations (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE configurations ADD user_id INT DEFAULT NULL, ADD date DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE configurations ADD CONSTRAINT FK_31C6AD9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_31C6AD9A76ED395 ON configurations (user_id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6496F0A14EE');
        $this->addSql('DROP INDEX IDX_8D93D6496F0A14EE ON user');
        $this->addSql('ALTER TABLE user DROP configurations_id');
    }
}
