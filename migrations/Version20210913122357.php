<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210913122357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE config (id INT AUTO_INCREMENT NOT NULL, motherboard VARCHAR(255) DEFAULT NULL, motherboard_url VARCHAR(255) DEFAULT NULL, ram VARCHAR(255) DEFAULT NULL, ram_url VARCHAR(255) DEFAULT NULL, cpu VARCHAR(255) DEFAULT NULL, cpu_url VARCHAR(255) DEFAULT NULL, powersupply VARCHAR(255) DEFAULT NULL, powersupply_url VARCHAR(255) DEFAULT NULL, gpus NUMERIC(10, 0) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE config');
    }
}
