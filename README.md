<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]



<!-- PROJECT LOGO -->
<br />
<p>
  <a href="hhttps://gitlab.com/keziah.nivelle.simplon/coinshire">
    <img src="public/img/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Coinshire</h3>

   <p align="center">
    An awesome README template to jumpstart your projects!
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
  </p>
</p>


<details open="open">
  <summary>Table of Contents</summary>
    <ul>
        <li>
        <a href="#about-the-project">About The Project</a>
        </li>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li> <a href="#getting-started">Getting Started</a></li>
        <li><a href="#about-the-project">About The Project</a></li>
        <li><a href="#contact">Contact</a></li>
    </ul>
</details>

## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

### This project is an application to help novice cryptocurrency miners calculate their returns.

##### Part 1

The first part of this project focuses on the analysis of the cryptocurrency market.
There, users can find detailed information about the cryptos of their choice. For example, the current exchange rate, price changes, the number of units in circulation and a lot of other information. Finally, they can have the conversion value of the selected currency.
By staying on this first part,the users can also access a list of the different exchange platforms. And as with cryptos, they have access to a lot of details and a “trust score”.
This first part was mainly developed with JavaScript. To display all the data I used the API and the widgets from coingecko.com.

##### Part 2

The second part of the project is more dedicated to cryptocurrency mining.
Mining is an operation that consists of validating a transaction by encrypting the data and recording it in the blockchain using the power of processors, computers or graphics cards.
On this part users can select the model of their graphics card and know the daily and monthly profits they can make with it.

Finally, users have access to a list of configuration and components tested and approved to be able to mine.
For this part I used an API giving me information on the main cryptocurrency algorithms.



### Built With

* [Symfony](https://symfony.com/)
* [JavaScript](hhttps://developer.mozilla.org/fr/docs/Web/JavaScript)
* [Bootstrap](https://getbootstrap.com/)
* [Less](https://lesscss.org/)



## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/keziah.nivelle.simplon/coinshire.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Install composer packages
   ```sh
   composer install
   ```
4. Update composer packages
   ```sh
   composer update
   ```
5. Start serve
   ```sh
   cd public
   ```
6. Start serve
   ```sh
   php -S localhost:8080
   ```


## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

NIVELLE Keziah - km.nivelle@gmail.com

Project Link: [https://gitlab.com/keziah.nivelle.simplon/coinshire](https://gitlab.com/keziah.nivelle.simplon/coinshire)


[issues-url]: hhttps://gitlab.com/keziah.nivelle.simplon/coinshire/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: hhttps://gitlab.com/keziah.nivelle.simplon/coinshire/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/keziah-nivelle/
[product-screenshot]: public/img/Coinshire1.png


