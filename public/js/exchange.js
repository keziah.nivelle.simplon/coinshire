/// Fetch the api and sort the exenges info //

async function returnExcanges(){
    let Url = ('https://api.coingecko.com/api/v3/exchanges');
    let resultat = await fetch(Url);
    let excanges = await resultat.json();
    var arrayLength = excanges.length;
    
    for (var i = 0; i < arrayLength; i++) { 
        
        var exchangesTable = document.getElementById("exchangesTable");

        exchangesTable.innerHTML += '<td>' + excanges[i].trust_score_rank +  '</td>'
         +'<td>' + "<img src=" + excanges[i].image +" >" +  '</td>'
         +'<td>' + '<a href=' + excanges[i].id + '>' +  excanges[i].name  + "</a>"  + '</td>'
         +'<td>' + excanges[i].trust_score +  '</td>'
         +'<td>' + excanges[i].year_established +  '</td>';
              
    };    
}

returnExcanges();


// Get info foreach exanges info


async function returnOneExcange(){
    let Url = ('https://api.coingecko.com/api/v3/exchanges' + window.location.pathname); //Url + id from  url 
    let resultat = await fetch(Url);
    let datas = await resultat.json();
    var arrayLength = datas.length;
    // console.log(datas.name)
    
    var exangeName = document.getElementById("exangeName");
    var score = document.getElementById("score");
    var link = document.getElementById("link");
    var excangeVolume = document.getElementById("excangeVolume");
    var excangeCreat = document.getElementById("excangeCreat");
    var excangeFrom = document.getElementById("excangeFrom");
    var statu = document.getElementById("statu");
    var statuAuthor = document.getElementById("statuAuthor");
    var statuDate = document.getElementById("statuDate");
   
    exangeName.innerHTML += "<img class='me-3' src=" + datas.image +" >" + datas.name;
     score.innerHTML += 'Score : ' + datas.trust_score + '/10';
     link.innerHTML += '<a class="btn btn-primary" href=' + datas.url + '>' +  'Visiter' + "</a>"; 
     excangeVolume.innerHTML += datas.trade_volume_24h_btc; 
     excangeCreat.innerHTML += datas.year_established; 
     excangeFrom.innerHTML += datas.country; 
     statu.innerHTML += datas.status_updates[0].description; 
     statuAuthor.innerHTML += 'Auteur: ' + datas.status_updates[0].user; 
     statuDate.innerHTML += datas.status_updates[0].created_at; 
    
}

returnOneExcange();