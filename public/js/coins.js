/// Get api coins info for coingecko api//
   

async function returnCoin(){
    let Url = ('https://api.coingecko.com/api/v3/coins');
    let resultat = await fetch(Url);
    let coins = await resultat.json();
    var arrayLength = coins.length;
    coins.forEach(element => (element));


  
    for (var i = 0; i < arrayLength; i++) { 
        var table = document.getElementById("coinsTable"); // var for print result in the table
        table.innerHTML += '<td>' + coins[i].market_data.market_cap_rank +  '</td>'
         +'<td>' + "<img src=" + coins[i].image.thumb +" >" +  '</td>'
         +'<td>' + '<a href=' + coins[i].id + '>' +  coins[i].id+ "</a>"  + '</td>'
         +'<td>' + coins[i].market_data.current_price.eur + CurrencyCode + '</td>'
         +'<td>' + '<div class=' + "'hour'" +  ' >' +coins[i].market_data.price_change_percentage_1h_in_currency.eur.toFixed(2) + ' %' + '</td>'
         +'<td ' + '<id=' + 'day' +  ' >' + coins[i].market_data.price_change_percentage_24h_in_currency.eur.toFixed(2) + ' %' + '</td>'
         +'<td>' + coins[i].market_data.price_change_percentage_7d_in_currency.eur.toFixed(2) + ' %' + '</td>'
         +'<td>' + coins[i].market_data.price_change_percentage_30d_in_currency.eur.toFixed(2) + ' %' + '</td>'
         +'<td>'  + coins[i].market_data.market_cap.eur.toFixed(2).toLocaleString("fi-FI")+ CurrencyCode + '</td>';

    };    

}

returnCoin();


/// Single coin view ///


async function returnOneCoin(){ // Fetch for get coins info by id 
    let Url = ('https://api.coingecko.com/api/v3/coins/' + window.location.pathname); //Url + id on the url 
    let resultat = await fetch(Url);
    let data = await resultat.json();
    var arrayLength = data.length;
  
    var $ = document.querySelector.bind(document)
    
    var name = $("#name"),
    rank = $("#rank"),
    price = $("#price"),
    title = $("#Title"),
     priceTable = $("#priceTable"),
     marketCapTable = $("#marketCapTable"),
     capDominanceTable = $("#capDominanceTable"),
     traidVolumeTable = $("#traidVolumeTable"),
     volumeMArketCappriceTable = $("#volumeMArketCappriceTable"),
     dayTable = $("#dayTable"),
     weekTable = $("#weekTable"),
     RankTable = $("#RankTable"),
     highTable = $("#highTable"),
     LowTable = $("#LowTable"),
     description = $("#description");
     website = $("#Website"),
     Explorers = $("#Explorers"),
     Community = $("#Community"),
     Genegis = $("#Genegis"),
     SourceCode = $("#SourceCode"),
     Vmc = data.market_data.market_cap.eur.toFixed(2) / data.market_data.total_volume.eur.toFixed(2),
     chart = $ ("#chart");
     Exenges = $("#Exenges"),
     conver = $ ("#convert");
  


    name.innerHTML +=   "<img class='me-3' src=" + data.image.small +" >" + data.name + '<br>' + '<em class="price ">' + data.market_data.current_price.eur  + ' € ' + '</em>'+ '<br>' + ' <em class="rank">' +' Rank # '+data.market_cap_rank +'</em>' ; 
    priceTable.innerHTML += data.market_data.current_price.eur.toFixed(2)  + CurrencyCode ;
    marketCapTable.innerHTML += data.market_data.market_cap.eur  + CurrencyCode ;
    capDominanceTable.innerHTML += data.market_data.market_cap.eur + CurrencyCode;
    traidVolumeTable.innerHTML += data.market_data.total_volume.eur + CurrencyCode;
    volumeMArketCappriceTable.innerHTML += Vmc.toFixed(2) + UnitType ;
    dayTable.innerHTML += data.market_data.price_change_percentage_7d + UnitType ;
    weekTable.innerHTML += data.market_data.price_change_percentage_14d + UnitType;
    RankTable.innerHTML += data.market_cap_rank ;
    highTable.innerHTML += data.market_data.low_24h.eur  + CurrencyCode;
    LowTable.innerHTML += data.market_data.high_24h.eur   + CurrencyCode;
    title.innerHTML += 'À propos de ' + data.name;
    description.innerHTML += data.description.fr;
    website.innerHTML +=  '<a href=' + data.links.homepage + '>' +  data.id + '.com'+ "</a>";
    Explorers.innerHTML += '<a href=' + data.links.blockchain_site[0] + '>' +  data.id+ "</a>";
    Community.innerHTML += '<a href=' + "https://twitter.com/" + data.links.twitter_screen_name  +  '>' + '<i class="fab fa-twitter "></i> ' + "</a>" + '<a href=' + "https://www.facebook.com/"  + data.links.facebook_username  + '>' + '  <i class="fab fa-facebook"></i>'  + "</a>" + '<a href=' + data.links.subreddit_url  + '>' +  ' <i class="fab fa-reddit-alien"></i>'   ;
    SourceCode.innerHTML += '<a href=' +  data.links.repos_url.github[0]  +  '>' + '<i class="fab fa-github"></i> ';
    Genegis.innerHTML += data.genesis_date;

    // Cryptocurency convert 

    convert.innerHTML += '<coingecko-coin-converter-widget  coin-id="' + window.location.pathname +'" currency="eur" background-color="#ffffff" font-color="#03045e" locale="fr"></coingecko-coin-converter-widget>';

    // Chart 
    chart.innerHTML += '<coingecko-coin-compare-chart-widget  coin-ids=' + window.location.pathname + '  currency="eur" locale="fr" font-color="#03045e" locale="fr"></coingecko-coin-compare-chart-widget>';

    // Exenges 

    Exenges .innerHTML += '<coingecko-coin-market-ticker-list-widget  coin-id=' + window.location.pathname + '  currency="eur" locale="fr" font-color="#03045e" locale="fr"></coingecko-coin-market-ticker-list-widget>';

}
returnOneCoin();
