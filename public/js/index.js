var CurrencyCode = ' €'
var UnitType = ' % '


/// Get api global info  ///

async function returnGlobal(){
    let Url = ('https://api.coingecko.com/api/v3/global');
    let resultat = await fetch(Url);
    let data = await resultat.json();
    var arrayLength = data.length;


    // Inner result in the banner 
    var scrolling = document.getElementById("scrolling");
    scrolling.innerHTML += "<div>" + ' <b> Cryptomonnaies actives : </b>' 
    + '<em> ' + data.data.active_cryptocurrencies + '</em> ' + " <b> Plaformes d'échanges actives : </b>  <em> 492 </em> "+ '<b> Capitalisation du marché : </b>' + 
    '<em> ' + data.data.total_market_cap.eur  + ' € ' + '</em>'  + '<b> Vol. 24 h: </b> ' + '<em>' + data.data.total_volume.eur  
    + ' € ' + '</em>' + ' <b> Prédominance </b> ' + ' <i class="fab fa-bitcoin"></i> - BTC : ' + '<em>' + data.data.market_cap_percentage.btc.toFixed(2) + '</em>' + UnitType + '   <i class="fab fa-ethereum"></i> - ETH: ' + ' <em> ' + data.data.market_cap_percentage.eth.toFixed(2) + ' </em> ' + UnitType + "</div>";

}
returnGlobal();


/// dark mode ///
function dark() {
    var element = document.body;
    element.classList.toggle("dark-mode");
 }



async function returnTrending(){
    let Url = ('https://api.coingecko.com/api/v3/search/trending');
    let resultat = await fetch(Url);
    let trending = await resultat.json();
    var arrayLength = trending.length;
    // console.log(trending.coins[1].item.name)

    for (var i = 0; i < arrayLength; i++) { 
        
        console.log(trending.coins[i].item.name)

        
    };    
}

returnTrending();
