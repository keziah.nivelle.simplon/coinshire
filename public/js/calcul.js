// Change value if the minestat api coins order change /
// https://api.minerstat.com/v2/coins /
var ethtab = 115; // Ether
var etctab = 114; // Ether C
var rvntab = 419; // Ravencoin

/// ETH / Roi / ///

function getListValue() {
    var selectedOption,
        st;

    selectedOption = document
        .getElementById("myList")
        .selectedIndex;
    st = document
        .getElementById("myList")
        .options[selectedOption]
        .value;

    async function returnCalcul() {
        let Url = ('https://api.minerstat.com/v2/coins');
        let resultat = await fetch(Url);
        let obj = await resultat.json();
        obj.forEach(element => (element));
        var arrayLength = obj.length;
        placeholder = "Username"
        mswCoin = obj[ethtab].coin; // Get eth value from the Api
        mswName = obj[ethtab].name;
        mswAlgo = obj[ethtab].algorithm; // Get eth algo
        mswHashrate = obj[ethtab].network_hashrate; // Get teh curent network hrate
        mswDifficulty = obj[ethtab].difficulty; // The actual dificulty
        mswReward = obj[ethtab].reward; // The curent reward
        mswRewardUnit = obj[ethtab].reward_unit; // The reward unit
        mswRewardBlock = obj[ethtab].reward_block; // THe reward per block
        mswPrice = obj[ethtab].price; // Eth price
        mswType = obj[ethtab].type; // Eth type
        mswRewards = (mswReward * Math.abs(parseFloat(st))) * 1000000 * 24; // Reward * MH/s * 1M * 24 (1 day)
        mswEarnings = mswRewards * mswPrice; // (Reward * MH/s * 1M * 24 )* Eth price
        document
            .getElementById("myTextBoxResult")
            .value = mswEarnings.toFixed(2);
        document
            .getElementById("myTextBoxResultweek")
            .value = (mswEarnings * 7).toFixed(2);
        document
            .getElementById("myTextBoxResultmouth")
            .value = (mswEarnings * 30).toFixed(2);

    }

    returnCalcul();

}

/// ETC /

function getListValueEtc() {
    var selectedOption,
        etcHrate;
    selectedOption = document
        .getElementById("myListEtc")
        .selectedIndex;
    etcHrate = document
        .getElementById("myListEtc")
        .options[selectedOption]
        .value
        console
        .log(etcHrate)

    async function returnCalculEtc() {
        let Url = ('https://api.minerstat.com/v2/coins');
        let resultat = await fetch(Url);
        let obj = await resultat.json();
        obj.forEach(element => (element));
        var arrayLength = obj.length;
        placeholder = "Username"
        mswCoin = obj[etctab].coin; // Get eth value from the Api
        mswName = obj[etctab].name;
        mswAlgo = obj[etctab].algorithm; // Get eth algo
        mswHashrate = obj[etctab].network_hashrate; // Get teh curent network hrate
        mswDifficulty = obj[etctab].difficulty; // The actual dificulty
        mswReward = obj[etctab].reward; // The curent reward
        mswRewardUnit = obj[etctab].reward_unit; // The reward unit
        mswRewardBlock = obj[etctab].reward_block; // THe reward per block
        mswPrice = obj[etctab].price; // Eth price
        mswType = obj[etctab].type; // Eth type
        mswRewards = mswReward * Math.abs(parseFloat(etcHrate)) * 1000000 * 24; // Reward * MH/s * 1M * 24 (1 day)
        mswEarnings = mswRewards * mswPrice; // (Reward * MH/s * 1M * 24 )* Eth price
        document
            .getElementById("myTextBoxResultEtc")
            .value = mswEarnings.toFixed(2);
        document
            .getElementById("myTextBoxResultweekEtc")
            .value = (mswEarnings * 7).toFixed(2);
        document
            .getElementById("myTextBoxResultmouthEtc")
            .value = (mswEarnings * 30).toFixed(2);
    }
    returnCalculEtc();

}

/// RAVENCOIN /

function getListValueRvn() {
    var selectedOption,
        rvnHrate;
    selectedOption = document
        .getElementById("myListRvn")
        .selectedIndex;
    rvnHrate = document
        .getElementById("myListRvn")
        .options[selectedOption]
        .value
        console
        .log(rvnHrate)

    async function returnCalculRvn() {
        let Url = ('https://api.minerstat.com/v2/coins');
        let resultat = await fetch(Url);
        let obj = await resultat.json();
        obj.forEach(element => (element));
        var arrayLength = obj.length;
        placeholder = "Username"
        mswCoin = obj[rvntab].coin; // Get eth value from the Api
        mswName = obj[rvntab].name;
        mswAlgo = obj[rvntab].algorithm; // Get eth algo
        mswHashrate = obj[rvntab].network_hashrate; // Get teh curent network hrate
        mswDifficulty = obj[rvntab].difficulty; // The actual dificulty
        mswReward = obj[rvntab].reward; // The curent reward
        mswRewardUnit = obj[rvntab].reward_unit; // The reward unit
        mswRewardBlock = obj[rvntab].reward_block; // THe reward per block
        mswPrice = obj[rvntab].price; // Eth price
        mswType = obj[rvntab].type; // Eth type
        mswRewards = mswReward * Math.abs(parseFloat(rvnHrate)) * 1000000 * 24; // Reward * MH/s * 1M * 24 (1 day)
        mswEarnings = mswRewards * mswPrice; // (Reward * MH/s * 1M * 24 )* Eth price
        document
            .getElementById("myTextBoxResultRvn")
            .value = mswEarnings.toFixed(2);
        document
            .getElementById("myTextBoxResultweekRvn")
            .value = (mswEarnings * 7).toFixed(2);
        document
            .getElementById("myTextBoxResultmouthRvn")
            .value = (mswEarnings * 30).toFixed(2);
    }
    returnCalculRvn();

}
