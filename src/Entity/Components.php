<?php

namespace App\Entity;

use App\Repository\ComponentsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Mapping\ClassMetadata;


/**
 * @ORM\Entity(repositoryClass=ComponentsRepository::class)
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 */
class Components
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Url;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $Price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $Frequency;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $Power;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $Pcies;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Socket;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $Size;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $img;

    

    

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->Url;
    }

    public function setUrl(string $Url): self
    {
        $this->Url = $Url;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->Price;
    }

    public function setPrice(?string $Price): self
    {
        $this->Price = $Price;

        return $this;
    }

    public function getFrequency(): ?string
    {
        return $this->Frequency;
    }

    public function setFrequency(?string $Frequency): self
    {
        $this->Frequency = $Frequency;

        return $this;
    }

    public function getPower(): ?string
    {
        return $this->Power;
    }

    public function setPower(?string $Power): self
    {
        $this->Power = $Power;

        return $this;
    }

    public function getPcies(): ?string
    {
        return $this->Pcies;
    }

    public function setPcies(?string $Pcies): self
    {
        $this->Pcies = $Pcies;

        return $this;
    }

    public function getSocket(): ?string
    {
        return $this->Socket;
    }

    public function setSocket(?string $Socket): self
    {
        $this->Socket = $Socket;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->Size;
    }

    public function setSize(?string $Size): self
    {
        $this->Size = $Size;

        return $this;
    }

    

    public function __toString(): string
    {
        return $this->Name;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

}
