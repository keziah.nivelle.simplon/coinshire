<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gpu
 *
 * @ORM\Table(name="gpu")
 * @ORM\Entity
 */
class Gpu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="marketingname", type="string", length=29, nullable=true)
     */
    private $marketingName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vendor", type="string", length=6, nullable=true)
     */
    private $vendor;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ramSize", type="integer", nullable=true)
     */
    private $ramsize;

    /**
     * @var int|null
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var float|null
     *
     * @ORM\Column(name="secondHand", type="float", precision=10, scale=0, nullable=true)
     */
    private $secondhand;

    /**
     * @var float|null
     *
     * @ORM\Column(name="cuckatoo", type="float", precision=10, scale=0, nullable=true)
     */
    private $cuckatoo;

    /**
     * @var float|null
     *
     * @ORM\Column(name="cuckooCycle", type="float", precision=10, scale=0, nullable=true)
     */
    private $cuckoocycle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cuckooCortex", type="integer", nullable=true)
     */
    private $cuckoocortex;

    /**
     * @var int|null
     *
     * @ORM\Column(name="equihash", type="integer", nullable=true)
     */
    private $equihash;

    /**
     * @var int|null
     *
     * @ORM\Column(name="beamHash", type="integer", nullable=true)
     */
    private $beamhash;

    /**
     * @var int|null
     *
     * @ORM\Column(name="etchash", type="integer", nullable=true)
     */
    private $etchash;

    /**
     * @var float|null
     *
     * @ORM\Column(name="mtp", type="float", precision=10, scale=0, nullable=true)
     */
    private $mtp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="kawpow", type="integer", nullable=true)
     */
    private $kawpow;

    /**
     * @var float|null
     *
     * @ORM\Column(name="randomX", type="float", precision=10, scale=0, nullable=true)
     */
    private $randomx;

    /**
     * @var int|null
     *
     * @ORM\Column(name="eaglesong", type="integer", nullable=true)
     */
    private $eaglesong;

    /**
     * @var int|null
     *
     * @ORM\Column(name="autolykos", type="integer", nullable=true)
     */
    private $autolykos;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMarketingName(): ?string
    {
        return $this->marketingName;
    }

    public function setMarketingName(?string $marketingName): self
    {
        $this->marketingName = $marketingName;

        return $this;
    }

    public function getVendor(): ?string
    {
        return $this->vendor;
    }

    public function setVendor(?string $vendor): self
    {
        $this->vendor = $vendor;

        return $this;
    }

    public function getRamsize(): ?int
    {
        return $this->ramsize;
    }

    public function setRamsize(?int $ramsize): self
    {
        $this->ramsize = $ramsize;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSecondhand(): ?float
    {
        return $this->secondhand;
    }

    public function setSecondhand(?float $secondhand): self
    {
        $this->secondhand = $secondhand;

        return $this;
    }

    public function getCuckatoo(): ?float
    {
        return $this->cuckatoo;
    }

    public function setCuckatoo(?float $cuckatoo): self
    {
        $this->cuckatoo = $cuckatoo;

        return $this;
    }

    public function getCuckoocycle(): ?float
    {
        return $this->cuckoocycle;
    }

    public function setCuckoocycle(?float $cuckoocycle): self
    {
        $this->cuckoocycle = $cuckoocycle;

        return $this;
    }

    public function getCuckoocortex(): ?int
    {
        return $this->cuckoocortex;
    }

    public function setCuckoocortex(?int $cuckoocortex): self
    {
        $this->cuckoocortex = $cuckoocortex;

        return $this;
    }

    public function getEquihash(): ?int
    {
        return $this->equihash;
    }

    public function setEquihash(?int $equihash): self
    {
        $this->equihash = $equihash;

        return $this;
    }

    public function getBeamhash(): ?int
    {
        return $this->beamhash;
    }

    public function setBeamhash(?int $beamhash): self
    {
        $this->beamhash = $beamhash;

        return $this;
    }

    public function getEtchash(): ?int
    {
        return $this->etchash;
    }

    public function setEtchash(?int $etchash): self
    {
        $this->etchash = $etchash;

        return $this;
    }

    public function getMtp(): ?float
    {
        return $this->mtp;
    }

    public function setMtp(?float $mtp): self
    {
        $this->mtp = $mtp;

        return $this;
    }

    public function getKawpow(): ?int
    {
        return $this->kawpow;
    }

    public function setKawpow(?int $kawpow): self
    {
        $this->kawpow = $kawpow;

        return $this;
    }

    public function getRandomx(): ?float
    {
        return $this->randomx;
    }

    public function setRandomx(?float $randomx): self
    {
        $this->randomx = $randomx;

        return $this;
    }

    public function getEaglesong(): ?int
    {
        return $this->eaglesong;
    }

    public function setEaglesong(?int $eaglesong): self
    {
        $this->eaglesong = $eaglesong;

        return $this;
    }

    public function getAutolykos(): ?int
    {
        return $this->autolykos;
    }

    public function setAutolykos(?int $autolykos): self
    {
        $this->autolykos = $autolykos;

        return $this;
    }

 
    public function __toString(): string
    {
        return $this->Name;
    }

}
