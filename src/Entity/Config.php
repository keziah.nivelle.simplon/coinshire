<?php

namespace App\Entity;

use App\Repository\ConfigRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConfigRepository::class)
 */
class Config
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Motherboard;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Motherboard_Url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Ram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Ram_Url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Cpu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Cpu_Url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $powersupply;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $powersupply_url;

    

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $Gpus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Cooler;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Cooler_Url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Title;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $Price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMotherboard(): ?string
    {
        return $this->Motherboard;
    }

    public function setMotherboard(?string $Motherboard): self
    {
        $this->Motherboard = $Motherboard;

        return $this;
    }

    public function getMotherboardUrl(): ?string
    {
        return $this->Motherboard_Url;
    }

    public function setMotherboardUrl(?string $Motherboard_Url): self
    {
        $this->Motherboard_Url = $Motherboard_Url;

        return $this;
    }

    public function getRam(): ?string
    {
        return $this->Ram;
    }

    public function setRam(?string $Ram): self
    {
        $this->Ram = $Ram;

        return $this;
    }

    public function getRamUrl(): ?string
    {
        return $this->Ram_Url;
    }

    public function setRamUrl(?string $Ram_Url): self
    {
        $this->Ram_Url = $Ram_Url;

        return $this;
    }

    public function getCpu(): ?string
    {
        return $this->Cpu;
    }

    public function setCpu(?string $Cpu): self
    {
        $this->Cpu = $Cpu;

        return $this;
    }

    public function getCpuUrl(): ?string
    {
        return $this->Cpu_Url;
    }

    public function setCpuUrl(?string $Cpu_Url): self
    {
        $this->Cpu_Url = $Cpu_Url;

        return $this;
    }

    public function getPowersupply(): ?string
    {
        return $this->powersupply;
    }

    public function setPowersupply(?string $powersupply): self
    {
        $this->powersupply = $powersupply;

        return $this;
    }

    public function getPowersupplyUrl(): ?string
    {
        return $this->powersupply_url;
    }

    public function setPowersupplyUrl(?string $powersupply_url): self
    {
        $this->powersupply_url = $powersupply_url;

        return $this;
    }

    

    public function getGpus(): ?string
    {
        return $this->Gpus;
    }

    public function setGpus(?string $Gpus): self
    {
        $this->Gpus = $Gpus;

        return $this;
    }

    public function getCooler(): ?string
    {
        return $this->Cooler;
    }

    public function setCooler(?string $Cooler): self
    {
        $this->Cooler = $Cooler;

        return $this;
    }

    public function getCoolerUrl(): ?string
    {
        return $this->Cooler_Url;
    }

    public function setCoolerUrl(?string $Cooler_Url): self
    {
        $this->Cooler_Url = $Cooler_Url;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(?string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->Price;
    }

    public function setPrice(?string $Price): self
    {
        $this->Price = $Price;

        return $this;
    }
}
