<?php

namespace App\Form;

use App\Entity\Components;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ComponentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Type', ChoiceType::class, [
                'choices' => [
                    'Gpu' => 'Gpu',
                    'Cpu' => 'Cpu',
                    'Ram' => 'Ram',
                    'Storage' => 'Storage',
                    'Motherboard' => 'Motherboard',
                    'Powersupply' => 'Powersupply',
                    'Accessory' => 'Accessory',
                    'Rig' => 'Rig',

                ],
            ])
            ->add('Name')
            ->add('Url')

            ->add('Price',  null, [
                'required'   => false,
                'empty_data' => '0',
            ])

            ->add('Size',  null, [
                'required'   => false,
                'empty_data' => '0',
            ])

            ->add('Frequency',  null, [
                'required'   => false,
                'empty_data' => '0',
            ])

            ->add('Power',  null, [
                'required'   => false,
                'empty_data' => '0',
            ])

            ->add('Pcies',  null, [
                'required'   => false,
                'empty_data' => '0',
            ])

            ->add('Socket',  null, [
                'required'   => false,
                'empty_data' => '0',
            ])

            ->add('Img',  null, [
                'required'   => false,
                'empty_data' => '0',
            ])
        ;
    }

    // public function configureOptions(OptionsResolver $resolver)
    // {
    //     $resolver->setDefaults([
    //         'data_class' => Components::class,
    //     ]);
    // }
}
