<?php

namespace App\Form;

use App\Entity\Gpu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GpuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('marketingName')
            ->add('vendor')
            ->add('ramsize')
            ->add('price')
            ->add('secondhand')
            ->add('cuckatoo')
            ->add('cuckoocycle')
            ->add('cuckoocortex')
            ->add('equihash')
            ->add('beamhash')
            ->add('etchash')
            ->add('mtp')
            ->add('kawpow')
            ->add('randomx')
            ->add('eaglesong')
            ->add('autolykos')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gpu::class,
        ]);
    }
}
