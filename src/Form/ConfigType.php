<?php

namespace App\Form;

use App\Entity\Config;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Title')
            ->add('Motherboard')
            ->add('Motherboard_Url')
            ->add('Ram')
            ->add('Ram_Url')
            ->add('Cpu')
            ->add('Cpu_Url')
            ->add('powersupply')
            ->add('powersupply_url')
            ->add('Cooler')
            ->add('Cooler_Url')
            ->add('Price')
            ->add('Gpus')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Config::class,
        ]);
    }
}
