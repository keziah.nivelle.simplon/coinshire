<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExangeViewController extends AbstractController
{
    /**
     * @Route("/binance", name="binance")
     * @Route("/gdax", name="gdax")
     * @Route("/ftx_spot", name="ftx_spot")
     * @Route("/crypto_com", name="crypto_com")
     * @Route("/kraken", name="kraken")
     * @Route("/bitfinex", name="bitfinex")
     * @Route("/binance_us", name="binance_us")
     * @Route("/gate", name="gate")
     * @Route("/gemini", name="gemini")
     * @Route("/bitmax", name="bitmax")
     */
    public function index(): Response
    {
        return $this->render('exchanges/singleview.html.twig', [
            'controller_name' => 'ExangeViewController',
        ]);
    }
}
