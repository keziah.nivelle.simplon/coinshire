<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConfigomatiqueController extends AbstractController
{
    /**
     * @Route("/configomatique", name="configomatique")
     */
    public function index(): Response
    {
        return $this->render('configomatique/index.html.twig', [
            'controller_name' => 'ConfigomatiqueController',
        ]);
    }
}
