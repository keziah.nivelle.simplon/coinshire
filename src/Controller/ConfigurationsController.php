<?php

namespace App\Controller;

use App\Entity\Configurations;
use App\Form\ConfigurationsType;
use App\Repository\ConfigurationsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/configurations")
 *  Require ROLE_ADMIN for *every* controller method in this class.
 * @IsGranted("ROLE_ADMIN" )
 */
class ConfigurationsController extends AbstractController
{
    /**
     * @Route("/", name="configurations_index", methods={"GET"})
     */
    public function index(ConfigurationsRepository $configurationsRepository): Response
    {
        return $this->render('configurations/index.html.twig', [
            'configurations' => $configurationsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="configurations_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $configuration = new Configurations();
        $form = $this->createForm(ConfigurationsType::class, $configuration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($configuration);
            $entityManager->flush();

            return $this->redirectToRoute('configurations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('configurations/new.html.twig', [
            'configuration' => $configuration,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="configurations_show", methods={"GET"})
     */
    public function show(Configurations $configuration): Response
    {
        return $this->render('configurations/show.html.twig', [
            'configuration' => $configuration,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="configurations_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Configurations $configuration): Response
    {
        $form = $this->createForm(ConfigurationsType::class, $configuration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('configurations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('configurations/edit.html.twig', [
            'configuration' => $configuration,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="configurations_delete", methods={"POST"})
     */
    public function delete(Request $request, Configurations $configuration): Response
    {
        if ($this->isCsrfTokenValid('delete'.$configuration->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($configuration);
            $entityManager->flush();
        }

        return $this->redirectToRoute('configurations_index', [], Response::HTTP_SEE_OTHER);
    }
}
