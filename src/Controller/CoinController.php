<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoinController extends AbstractController
{
    /**
     * @Route("/bitcoin", name="bitcoin")
     * @Route("/ethereum", name="ethereum")
     * @Route("/cardano", name="cardano")
     * @Route("/binancecoin", name="binancecoin")
     * @Route("/tether", name="tether")
     * @Route("/ripple", name="ripple")
     * @Route("/dogecoin", name="dogecoin")
     * @Route("/solana", name="solana")
     * @Route("/polkadot", name="polkadot")
     * @Route("/usd-coin", name="usd-coin")
     * @Route("/uniswap", name="uniswap")
     *  @Route("/terra-luna", name="terra-luna")
     *  @Route("/binance-usd", name="binance-usd")
     *  @Route("/bitcoin-cash", name="bitcoin-cash")
     *  @Route("/chainlink", name="chainlink")
     *  @Route("/litecoin", name="litecoin")
     *  @Route("/kusama", name="kusama")
     *  @Route("/internet-computer", name="internet-computer")
     *  @Route("/wrapped-bitcoin", name="wrapped-bitcoin")
     *  @Route("/matic-network", name="matic-network")
     *  @Route("/ethereum-classic", name="ethereum-classic")
     *  @Route("/vechain", name="vechain")
     *  @Route("/stellar", name="stellar")
     *  @Route("/avalanche-2", name="avalanche-2")
     *  @Route("/filecoin", name="filecoin")
     *  @Route("/cosmos", name="cosmos")
     *  @Route("/theta-token", name="theta-token")
     *  @Route("/tron", name="tron")
     *  @Route("/compound-ether", name="compound-ether")
     *  @Route("/dai", name="dai")
     *  @Route("/okb", name="okb")
     *  @Route("/monero", name="monero")
     *  @Route("/pancakeswap-token", name="pancakeswap-token")
     *  @Route("ftx-token", name="ftx-token")
     *  @Route("/aave", name="aave")
     *  @Route("/compound-usd-coin", name="compound-usd-coin")
     *  @Route("/eos", name="eos")
     *  @Route("/tezos", name="tezos")
     *  @Route("/cdai", name="cdai")
     *  @Route("/the-graph", name="the-graph")
     *  @Route("/axie-infinity", name="axie-infinity")
     *  @Route("/crypto-com-chain", name="crypto-com-chain")
     *  @Route("/klay-token", name="klay-token")
     *  @Route("/algorand", name="algorand")
     *  @Route("/staked-ether", name="staked-ether")
     *  @Route("/neo", name="neo")
     *  @Route("/bitcoin-cash-abc-2", name="bitcoin-cash-abc-2")
     *  @Route("/shiba-inu", name="shiba-inu")
     *  @Route("/elrond-erd-2", name="elrond-erd-2")
     *  @Route("/maker", name="maker")
     *  @Route("/bitcoin-cash-sv", name="bitcoin-cash-sv")
     */
    public function index(): Response
    {
        return $this->render('coin/index.html.twig', [
            'controller_name' => 'CoinController',
        ]);
    }
}
