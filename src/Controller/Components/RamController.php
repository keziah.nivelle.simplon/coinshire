<?php

namespace App\Controller\Components;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Components;

class RamController extends AbstractController
{
    /**
     * @Route("component/ram", name="ram")
     */
    public function index(): Response
    {
        $Components = $this->getDoctrine()->getRepository(Components::class)->findBy([],['id' => 'asc']);

       
        return $this->render('Components/ram.html.twig', compact('Components'));
        
    }
}
