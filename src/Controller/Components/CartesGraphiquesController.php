<?php

namespace App\Controller\Components;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartesGraphiquesController extends AbstractController
{
    /**
     * @Route("/component/cartes_graphiques", name="cartes_graphiques")
     */
 
    public function index(): Response
    {
        $Components = $this->getDoctrine()->getRepository(Components::class)->findBy([],['id' => 'asc']);

       
        return $this->render('Components/cartesgraphiques.html.twig', compact('Components'));
        
    }
}
