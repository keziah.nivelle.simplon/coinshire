<?php

namespace App\Controller\Components;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Components;

class AccesoiresController extends AbstractController
{
    /**
     * @Route("/component/accesoires", name="accesoires")
     */

    public function index(): Response
    {
        $Components = $this->getDoctrine()->getRepository(Components::class)->findBy([],['id' => 'asc']);

       
        return $this->render('Components/accessoires.html.twig', compact('Components'));
        
    }
}
