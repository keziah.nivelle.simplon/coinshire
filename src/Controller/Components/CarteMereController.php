<?php

namespace App\Controller\Components;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Components;

class CarteMereController extends AbstractController
{
    /**
     * @Route("/component/cartemere", name="carte_mere")
     */
    

    public function index(): Response
    {
        $Components = $this->getDoctrine()->getRepository(Components::class)->findBy([],['id' => 'asc']);

       
        return $this->render('Components/cartesmere.html.twig', compact('Components'));
        
    }
}
