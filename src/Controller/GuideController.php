<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\News;


class GuideController extends AbstractController
{
    /**
     * @Route("/guide", name="guide")
     */
   
    public function index(): Response
    {
        $News = $this->getDoctrine()->getRepository(News::class)->findBy([],['id' => 'asc']);

       
        return $this->render('guide/index.html.twig', compact('News'));
        
    }
}
