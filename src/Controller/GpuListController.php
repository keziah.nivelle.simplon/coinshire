<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Config;

class GpuListController extends AbstractController
{
    /**
     * @Route("/category/gpu", name="gpu_list")
     */
    public function index(): Response
    {
        $Config = $this->getDoctrine()->getRepository(Config::class)->findBy([],['id' => 'asc']);

       
        return $this->render('configomatique/gpulist.html.twig', compact('Config'));
        
    }
}
