<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SingleConfigViewController extends AbstractController
{
    /**
     * @Route("/single/config/view", name="single_config_view")
     */
    public function index(): Response
    {
        return $this->render('single_config_view/index.html.twig', [
            'controller_name' => 'SingleConfigViewController',
        ]);
    }
    
}
