<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExchangesController extends AbstractController
{
    /**
     * @Route("/exchanges", name="exchanges")
     */
    public function index(): Response
    {
        return $this->render('exchanges/index.html.twig', [
            'controller_name' => 'ExchangesController',
        ]);
    }
}
