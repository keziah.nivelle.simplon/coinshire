<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Gpu;

class CalculatorController extends AbstractController
{
    /**
     * @Route("/calculator", name="calculator")
     */
    public function index(): Response
    {
        $Gpu = $this->getDoctrine()->getRepository(Gpu::class)->findBy([],['id' => 'asc']);

       
        return $this->render('calculator/index.html.twig', compact('Gpu'));
        
    }
    


}
