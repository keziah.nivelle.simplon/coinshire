<?php

namespace App\Controller;

use App\Entity\Gpu;
use App\Form\GpuType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/gpu")
 * Require ROLE_ADMIN for *every* controller method in this class.
 * @IsGranted("ROLE_ADMIN" )
 */
class GpuController extends AbstractController
{
    /**
     * @Route("/", name="gpu_index", methods={"GET"})
     */
    public function index(): Response
    {
        $gpus = $this->getDoctrine()
            ->getRepository(Gpu::class)
            ->findAll();

        return $this->render('gpu/index.html.twig', [
            'gpus' => $gpus,
        ]);
    }

    /**
     * @Route("/new", name="gpu_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $gpu = new Gpu();
        $form = $this->createForm(GpuType::class, $gpu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($gpu);
            $entityManager->flush();

            return $this->redirectToRoute('gpu_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gpu/new.html.twig', [
            'gpu' => $gpu,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="gpu_show", methods={"GET"})
     */
    public function show(Gpu $gpu): Response
    {
        return $this->render('gpu/show.html.twig', [
            'gpu' => $gpu,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="gpu_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Gpu $gpu): Response
    {
        $form = $this->createForm(GpuType::class, $gpu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gpu_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('gpu/edit.html.twig', [
            'gpu' => $gpu,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="gpu_delete", methods={"POST"})
     */
    public function delete(Request $request, Gpu $gpu): Response
    {
        if ($this->isCsrfTokenValid('delete'.$gpu->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($gpu);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gpu_index', [], Response::HTTP_SEE_OTHER);
    }
}
